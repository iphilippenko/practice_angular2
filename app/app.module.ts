import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpModule} from '@angular/http';

import { AppComponent }  from './app.component';
import {LecturesModule} from "./components/lectures/lectures.modules";
import {UsersComponent} from "./components/users/users";
import {UsersService} from "./components/users/users.service";
import {appRouting} from "./app.routes";

@NgModule({
  imports:      [
    BrowserModule,
    HttpModule,
    appRouting,
    LecturesModule
  ],
  declarations: [
    AppComponent,
    UsersComponent
  ],
  providers: [
    UsersService
  ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
