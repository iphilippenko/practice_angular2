import {ILectureDetails} from "./lecture-details.model";
import {Injectable} from '@angular/core';

@Injectable()
class LectureDetailsService {
  private lectures: ILectureDetails[] = [
    {
      id: 0,
      title: 'HTML part 1',
      number: 1,
      description: 'HTML Lecture Description',
      author: 'Maxim',
      time: '2h'
    },
    {
      id: 1,
      title: 'HTML part 2',
      number: 1,
      description: 'HTML Lecture Description',
      author: 'Maxim',
      time: '2h'
    },
    {
      id: 2,
      title: 'HTML part 2',
      number: 1,
      description: 'CSS Lecture Description',
      author: 'Maxim',
      time: '2h'
    }
  ];

  fetchLectureDetails(id: number): ILectureDetails{
    if (typeof id === 'undefined'){
      return null;
    }

    return this.findLectureById(id);
  }

  private findLectureById(id: number): ILectureDetails {
    return this.lectures.filter((item: ILectureDetails) => {
      return item.id === id;
    })[0];
  }
}


export default LectureDetailsService;
