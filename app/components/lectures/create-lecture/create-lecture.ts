import {Component, EventEmitter, Output, Input} from '@angular/core';
import {ILecture, LectureModel} from "./../lecture/lecture.model";

@Component({
  moduleId: module.id,
  selector: 'lecture-create',
  templateUrl: './create-lecture.html'
})

export class CreateLectureComponent {
  @Output()
  create: EventEmitter = new EventEmitter();
  @Output()
  cancel: EventEmitter<boolean> = new EventEmitter();

  @Input()
  lecture: ILecture;

  constructor(){}

  createLecture(): void {
    console.log(this.lecture);
    this.create.emit();
  }

  onCancel(): void {
    this.cancel.emit(true);
  }
}
